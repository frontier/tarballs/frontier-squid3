#!/bin/sh

DIST_DIR=$1
FILES_DIR=$2
DIST_LINK=$3
DIST_NAME=$4

cd $DIST_DIR

if [ -f $DIST_NAME ] ; then
 md5sum --status -c $FILES_DIR/$DIST_NAME.md5
 status=$?
 #echo $status
 if [ $status != 0 ] ; then
  echo "MD5 mismatch"
  rm -f $DIST_NAME
 else
  echo "Already exists"
  exit 0
 fi
fi

wget -O $DIST_NAME $DIST_LINK
status=$?
# echo $status
if [ $status != 0 ] ; then
 echo "Download failed."
 exit 1 
fi
touch .downloaded

md5sum --status -c $FILES_DIR/$DIST_NAME.md5
status=$?
#echo $status
if [ $status != 0 ] ; then
 rm -f $DIST_NAME
 echo "MD5 mismatch"
 exit 1
else
 echo "MD5 Ok"
 exit 0
fi

